function matchPerYear() {
    arrayOfMatchCount = [];
    fetch('./output/matchesPerYear.json')
    .then(response => response.json())
    .then(data => {
        for (key in data) {
            arrayOfMatchCount.push([key,data[key]]);
        }
        


        Highcharts.chart('matchPerYear', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of matches played per year for all the years in IPL'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Match Count'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'IPL matches: <b>{point.y:.1f}</b>'
            },
            series: [{
                name: 'Population',
                data: arrayOfMatchCount,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    })
    .catch((error) => {
        console.error(error);
    });
}

matchPerYear();



function matchWonPerTeamPerYear() {

    let arrayOfWin = [];
    fetch('./output/matchWonPerYearPerTeam.json')
    .then(response => response.json())
    .then((data) => {

        for (let year = 2008; year <= 2017; year ++) {
            tempObj = {};
            tempArr = [];
            for (keys in data) {
                if (data[keys][year]) {
                    tempArr.push(data[keys][year]);
                } else {
                    tempArr.push(0);
                }
            }
            tempObj.name = year;
            tempObj.data = tempArr;
            arrayOfWin.push(tempObj);
        }

        // console.log(arrayOfWin);


        Highcharts.chart('matchWonPerTeamPerYear', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Number of matches won per team per year in IPL'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle</a>'
            },
            xAxis: {
                categories: Object.keys(data),
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Matches',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ''
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: arrayOfWin
        });
    })
}


matchWonPerTeamPerYear();



function extraRun() {

    arrayOfExtraRuns = [];
    fetch('./output/extraRuns2016.json')
    .then(response => response.json())
    .then(data => {
        // console.log(data);

        for (keys in data) {
            arrayOfExtraRuns.push([keys, data[keys]]);
        }
        

        Highcharts.chart('extraRuns', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Extra runs conceded per team in the year 2016'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Extra runs'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Extra runs in 2016: <b>{point.y:.1f}</b>'
            },
            series: [{
                name: 'Population',
                data: arrayOfExtraRuns,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });


    });

}

extraRun();

function economicBowlers() {

    let bowlers = [];
    let economy = [];
    fetch('./output/economicalBowlers2015.json')
    .then(response => response.json())
    .then((data) => {



        for (key in data) {
            bowlers.push(key);
            economy.push(data[key]);
        }

        // console.log(Highcharts.getOptions().lang.shortMonths)
        // console.log(bowlers)
        // console.log(economy)


        Highcharts.chart('economicalBowlers2015', {
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 10,
                    beta: 25,
                    depth: 70
                }
            },
            title: {
                text: 'Top 10 economical bowlers in the year 2015'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle</a>'
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            xAxis: {
                categories: bowlers,
                labels: {
                    skew3d: true,
                    style: {
                        fontSize: '16px'
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            series: [{
                name: 'Economy of bowlers',
                data: economy
            }]
        });



        
    });
}

economicBowlers();



function tossAndMatchWinner() {

    let teamName = [];
    let tossAndWinCount = [];
    fetch('./output/tossAndMatchWinner.json')
    .then(response => response.json())
    .then((data) => {



        for (key in data) {
            teamName.push(key);
            tossAndWinCount.push(data[key]);
        }


        Highcharts.chart('tossAndMatchWinner', {
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 10,
                    beta: 25,
                    depth: 70
                }
            },
            title: {
                text: 'Number of times each team won the toss and also won the match'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle</a>'
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            xAxis: {
                categories: teamName,
                labels: {
                    skew3d: true,
                    style: {
                        fontSize: '16px'
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            series: [{
                name: 'Toss and win count',
                data: tossAndWinCount
            }]
        });
    })
}

tossAndMatchWinner();



// function awardWinners() {

//     let awardWinner = [];
//     let years = [];
//     fetch('./output/topAwardWinners.json')
//     .then(response => response.json())
//     .then((data) => {


//         for (keys in data) {
//             for (key in data[keys]) {
//                 awardWinner.push([key, data[keys][key]]);
//                 years.push(keys);
//             }
//         }

//         // console.log(awardWinner)


//         Highcharts.chart('awardWin', {

//             chart: {
//                 type: 'columnrange',
//                 inverted: true
//             },
        
//             accessibility: {
//                 description: ''
//             },
        
//             title: {
//                 text: 'Temperature variation by month'
//             },
        
//             subtitle: {
//                 text: 'Observed in Vik i Sogn, Norway, 2017'
//             },
        
//             xAxis: {
//                 categories: years
//             },
        
//             yAxis: {
//                 title: {
//                     text: 'Temperature ( °C )'
//                 }
//             },
        
//             tooltip: {
//                 valueSuffix: '°C'
//             },
        
//             plotOptions: {
//                 columnrange: {
//                     dataLabels: {
//                         enabled: true,
//                         format: '{y}°C'
//                     }
//                 }
//             },
        
//             legend: {
//                 enabled: false
//             },
        
//             series: [{
//                 name: 'Temperatures',
//                 data: awardWinner
//             }]
        
//         });
//     })
// }

// awardWinners();



function strikeRate() {

    let strikeRate = [];
    fetch('./output/strikeRateOfABatsman.json')
    .then(response => response.json())
    .then((data) => {


        for (keys in data) {
            strikeRate.push(data[keys]);
        }

        Highcharts.chart('strikeRate', {

            title: {
                text: 'Strike rate of Virat Kohli'
            },
        
            xAxis: {
                tickInterval: 1,
                type: 'logarithmic',
                accessibility: {
                    rangeDescription: 'Range: 1 to 10'
                }
            },
        
            yAxis: {
                type: 'logarithmic',
                minorTickInterval: 0.1,
                accessibility: {
                    rangeDescription: 'Range: 0.1 to 1000'
                }
            },
        
            tooltip: {
                // headerFormat: '<b>{series.name}</b><br />',
                pointFormat: 'Strike Rate = {point.y}'
            },
        
            series: [{
                data: strikeRate,
                pointStart: 2008
            }]
        });
    })
}
strikeRate();


function playerDismiss() {

    let dismissed = [];
    let bowler;
    fetch('./output/mostDismissedByAnotherPlayer.json')
    .then(response => response.json())
    .then((data) => {

        for (key in data) {
            for (k in data[key]) {
                dismissed.push([key, data[key][k]]);
                bowler = k;
            }
        }



        Highcharts.chart('dismiss', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Highest number of times one player has been dismissed by another player'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Dismissed count'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: `Dismissed by ${bowler}: <b>{point.y:.1f} times</b>`
            },
            series: [{
                name: 'Population',
                data: dismissed,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    })
}

playerDismiss();


function superOver() {

    let bowler = [];
    fetch('./output/bestEconomyInSuperOvers.json')
    .then(response => response.json())
    .then((data) => {



        for (key in data) {
            bowler.push([key, data[key]]);
        }

        Highcharts.chart('superOverEconomy', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Bowler with the best economy in super overs'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Economy'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: `Economy in Superover: <b>{point.y:.1f}</b>`
            },
            series: [{
                name: 'Population',
                data: bowler,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });


    })
}
superOver();