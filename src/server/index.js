const match = require('./ipl.js');

const Fs = require('fs');
const csv = require('csv-reader');

let match2015 = [];
let match2016 = [];
let matchIdPerYear = {};
let matchInput = Fs.createReadStream('../data/matches.csv', 'utf8');

matchInput
	.pipe(csv({parseNumbers: true}))

	.on('data', function (row) {
        match.callMatches(row, false);
        
        if (row[1] === 2015) {
            match2015.push(row[0]);
        }
        if (row[1] === 2016) {
            match2016.push(row[0]);
        }

        if (row[0] !== 'id') {
            matchIdPerYear[row[0]] = row[1];
        }
	});



let deliveriesInput = Fs.createReadStream('../data/deliveries.csv', 'utf8');

deliveriesInput
    .pipe(csv({parseNumbers: true}))

    .on('data', function(row) {
        match.callDeliveries(row, false, match2015, match2016, matchIdPerYear, false);
    })

    .on('end', function(row) {
        match.callMatches(row, true);
        match.callDeliveries(row, true);
    });
