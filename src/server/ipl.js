const fs = require('fs');

function matchPerYear() {
    let storeMatchCountPerYear = {};
    return function returnMatchPerYear(row,end) {
        if (!end) {
            if(storeMatchCountPerYear[row[1]]) {
                storeMatchCountPerYear[row[1]] += 1;
            } else if (row[1] !== 'season') {
                storeMatchCountPerYear[row[1]] = 1;
            }
        }
        else {
            fs.writeFile('../public/output/matchesPerYear.json', JSON.stringify(storeMatchCountPerYear, null, 4), err => {
                if (err) {
                  console.error(err)
                  return
                }
              })
        }
    }
}
let matchCount = matchPerYear();

function matchWonPerTeam() {
    let matchWinnerCounter = {};
    return function returnmatchWonPerTeam(row, end) {
        if (!end) {
            if (matchWinnerCounter[row[10]]) {
                if (matchWinnerCounter[row[10]][row[1]]) {
                    matchWinnerCounter[row[10]][row[1]] += 1;
                } else {
                    matchWinnerCounter[row[10]][row[1]] = 1;
                }
            } else if (row[8] !== "no result" && row[10] !== 'winner'){
                matchWinnerCounter[row[10]] = {};
                matchWinnerCounter[row[10]][row[1]] = 1;
            }
        } else {
            fs.writeFile('../public/output/matchWonPerYearPerTeam.json', JSON.stringify(matchWinnerCounter, null, 4), err => {
                if (err) {
                  console.error(err)
                  return
                }
              })
        }
    }
}

let matchWonCount = matchWonPerTeam();


function extraRuns() {
    let extraRunsCount = {};
    return function returnExtraRuns(row,arr,end) {
        if (!end) {
            if (arr.find(element => element === row[0])) {
                if (extraRunsCount[row[3]]) {
                    extraRunsCount[row[3]] += row[16];
                } else {
                    extraRunsCount[row[3]] = row[16];
                }
            }
        } else {
            fs.writeFile('../public/output/extraRuns2016.json', JSON.stringify(extraRunsCount, null, 4), err => {
                if (err) {
                  console.error(err)
                  return
                }
              })
        }
    }
}

let extraRunsInstance = extraRuns();

function economyOfBowlers() {
    let economyOfBowlers2015 = {};
    let oversOfBowlers = {};
    return function returnEconomyOfBowlers(row,arr,end) {
        if (!end) {
            if (arr.find(element => element === row[0])) {
                if(economyOfBowlers2015[row[8]]) {
                    economyOfBowlers2015[row[8]] += row[17];
                    oversOfBowlers[row[8]] += 1/6;
                } else {
                    economyOfBowlers2015[row[8]] = row[17];
                    oversOfBowlers[row[8]] = 1/6;
                }
            }
        } else {
            let arrayOfEconomy = [];
            let topEconomicalBowlers = {};

            for ( k in economyOfBowlers2015) {
                arrayOfEconomy.push([k, economyOfBowlers2015[k]/oversOfBowlers[k]]);
            }
            arrayOfEconomy.sort(([a, b], [c, d]) => b-d);
            for (let i = 0;i < 10; i++) {
                topEconomicalBowlers[arrayOfEconomy[i][0]] = arrayOfEconomy[i][1];
            }
            fs.writeFile('../public/output/economicalBowlers2015.json', JSON.stringify(topEconomicalBowlers, null, 4), err => {
                if (err) {
                  console.error(err)
                  return
                }
              })
        }
    }
}

let economyOfBowlersInstance = economyOfBowlers();

function tossAndMatch() {
    let tossAndMatchWinner = {};
    return (row, end) => {
        if(!end) {
            if (row[6] === row[10]) {
                if(tossAndMatchWinner[row[6]]) {
                    tossAndMatchWinner[row[6]] += 1;
                } else {
                    tossAndMatchWinner[row[6]] = 1;
                }
            }
        } else {
            fs.writeFile('../public/output/tossAndMatchWinner.json', JSON.stringify(tossAndMatchWinner, null, 4), err => {
                if (err) {
                  console.error(err)
                  return
                }
              })
        }
    }
}
let tossAndMatchInstance = tossAndMatch();


function playerOfMatchAward() {
    let playerOfMatch = {};
    return (row,end) => {
        if (!end) {
            if (playerOfMatch[row[1]]) {
                if (playerOfMatch[row[1]][row[13]]) {
                    playerOfMatch[row[1]][row[13]] += 1;
                } else if(row[13] !== '') {
                    playerOfMatch[row[1]][row[13]] =1;
                }
            } else if (row[1] !== 'season') {
                playerOfMatch[row[1]] = {};
                playerOfMatch[row[1]][row[13]] = 1;
            }
        } else {
            let matchAward = {};
            for (k in playerOfMatch) {
                
                temp = [];
                for (key in playerOfMatch[k]) {
                    temp.push([key, playerOfMatch[k][key]]);
                }
                temp.sort(([a, b], [c, d]) => d-b);
                matchAward[k] = {};
                max = temp[0][1];
                count = 0;
                while (temp[count][1] === max) {
                    matchAward[k][temp[count][0]] = temp[count][1];
                    count += 1;
                }
            }

            fs.writeFile('../public/output/topAwardWinners.json', JSON.stringify(matchAward, null, 4), err => {
                if (err) {
                  console.error(err)
                  return
                }
              })

        }
    }
}

let playerOfMatchAwardInstance = playerOfMatchAward();


function strikeRate() {
    let strikeRateOfBatsman = {};
    let totalRuns = {};
    let totalBowls = {};
    let batsman = "V Kohli";
    return (row, matchId, end) => {
        if (!end) {
            if(row[6] === batsman) {
                if (totalBowls[matchId[row[0]]]) {
                    totalBowls[matchId[row[0]]] += 1;
                    totalRuns[matchId[row[0]]] += row[15];
                } else {
                    totalBowls[matchId[row[0]]] = 1;
                    totalRuns[matchId[row[0]]] =row[15];
                }
            }
        } else {
            for (k in totalRuns) {
                strikeRateOfBatsman[k] = totalRuns[k] / totalBowls[k] * 100;
            }
            fs.writeFile('../public/output/strikeRateOfABatsman.json', JSON.stringify(strikeRateOfBatsman, null, 4), err => {
                if (err) {
                  console.error(err)
                  return
                }
              })
        }
    }
}

let strikeRateInstance = strikeRate();


function playerDismiss() {
    let playerDismissed = {};
    return (row, end) => {
        if (!end) {
            if (row[18] && row[18] !== '' && row[8] !== '') {
                if(playerDismissed[row[18]]) {
                    if (playerDismissed[row[18]][row[8]]) {
                        playerDismissed[row[18]][row[8]] += 1;   
                    } else {
                        playerDismissed[row[18]][row[8]] = 1;
                    }
                } else {
                    playerDismissed[row[18]] = {};
                    playerDismissed[row[18]][row[8]] = 1;
                }
            }
        }
        else {
            let topDismissed = {};
            let mostDismissed = {};
            for (k in playerDismissed) {
                let arrayOfDissmissed = [];
                for (key in playerDismissed[k]) {
                    arrayOfDissmissed.push([key, playerDismissed[k][key]]);
                }
                arrayOfDissmissed.sort(([a, b], [c, d]) => d-b);
                topDismissed[k] = {};
                topDismissed[k][arrayOfDissmissed[0][0]] = arrayOfDissmissed[0][1];
            }
            let tempArray = [];
            for (k in topDismissed) {
                for (key in topDismissed[k]) {
                    tempArray.push([k, [key, topDismissed[k][key]]]);
                }
            }
            tempArray.sort(([a, [b, c]], [d, [e, f]]) => f-c);
            mostDismissed[tempArray[0][0]] = {};
            mostDismissed[tempArray[0][0]][tempArray[0][1][0]] = tempArray[0][1][1];

            fs.writeFile('../public/output/mostDismissedByAnotherPlayer.json', JSON.stringify(mostDismissed, null, 4), err => {
                if (err) {
                console.error(err)
                return
                }
            })
        }
    }
}

let playerDismissInstance = playerDismiss();

function superOverEconomy() {
    let totalRunsSuperOver = {};
    let totalBowlsSuperOver = {};
    let economyInSuperovers = {};
    return (row, end) => {
        if(!end) {
            if (row[9] && row[8] !== 'bowler' && row[17] !== 'total_runs') {
                if(totalBowlsSuperOver[row[8]]) {
                    totalBowlsSuperOver[row[8]] += 1;
                    totalRunsSuperOver[row[8]] += row[17];
                } else {
                    totalRunsSuperOver[row[8]] = row[17];
                    totalBowlsSuperOver[row[8]] = 1;
                }
            }
        } else {
            for(key in totalBowlsSuperOver) {
                economyInSuperovers[key] = totalRunsSuperOver[key] / (totalBowlsSuperOver[key]/6);
            }
            let arrayOfEconomyInSuperOver = [];
            for (key in economyInSuperovers) {
                arrayOfEconomyInSuperOver.push([key, economyInSuperovers[key]]);
            }
            arrayOfEconomyInSuperOver.sort(([a, b], [c, d]) => b-d);
            let bestEconomyInSuperOvers = {};
            bestEconomyInSuperOvers[arrayOfEconomyInSuperOver[0][0]] = arrayOfEconomyInSuperOver[0][1];
        
            
            fs.writeFile('../public/output/bestEconomyInSuperOvers.json', JSON.stringify(bestEconomyInSuperOvers, null, 4), err => {
                if (err) {
                  console.error(err)
                  return
                }
              })
        }
    }
}

let superOverEconomyInstance = superOverEconomy();


function callMatches(row,end) {
    matchCount(row,end);
    matchWonCount(row,end);
    tossAndMatchInstance(row,end);
    playerOfMatchAwardInstance(row,end);
}
function callDeliveries(row, end, match2015, match2016, matchIdPerYear) {
    extraRunsInstance(row, match2016, end);
    economyOfBowlersInstance(row, match2015, end);
    strikeRateInstance(row, matchIdPerYear, end);
    playerDismissInstance(row, end);
    superOverEconomyInstance(row, end);
}
module.exports.callMatches = callMatches;
module.exports.callDeliveries = callDeliveries;